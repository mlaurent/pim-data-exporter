/*
   Copyright (C) 2012-2020 Laurent Montel <montel@kde.org>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; see the file COPYING.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef SELECTIONTYPEDIALOG_H
#define SELECTIONTYPEDIALOG_H

#include <QDialog>
#include "utils.h"
class SelectionTypeTreeWidget;
class QCheckBox;
class SelectionTypeDialog : public QDialog
{
    Q_OBJECT
public:
    explicit SelectionTypeDialog(bool backupData, QWidget *parent = nullptr);
    ~SelectionTypeDialog();

    Q_REQUIRED_RESULT QHash<Utils::AppsType, Utils::importExportParameters> storedType() const;

    void loadTemplate(const QString &fileName);

    Q_REQUIRED_RESULT QString exportedFileInfo() const;
    void removeNotSelectedItems();
private:
    void slotSelectAll();
    void slotUnselectAll();

    void slotSaveAsTemplate();
    void slotLoadTemplate();
    void readConfig();
    void writeConfig();
    void loadDefaultTemplate();
    void saveDefaultTemplate();
    SelectionTypeTreeWidget *mSelectionTreeWidget = nullptr;
    QCheckBox *mUseTemplateByDefault = nullptr;
    QPushButton *mSaveTemplate = nullptr;
    QPushButton *mLoadTemplate = nullptr;
};

#endif // SELECTIONTYPEDIALOG_H
