/*
   Copyright (C) 2020 Laurent Montel <montel@kde.org>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; see the file COPYING.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "exportimportutil.h"

ExportImportUtil::ExportImportUtil()
{
    mLogCreateResource = new LogCreatingResource(nullptr);
}

ExportImportUtil::~ExportImportUtil()
{
    delete mLogCreateResource;
}

void ExportImportUtil::setListOfResource(const QVector<Utils::AkonadiInstanceInfo> &instanceInfoList)
{
    mListAkonadiInstanceInfo = instanceInfoList;
}

void ExportImportUtil::setPathConfig(const QString &pathConfig)
{
    mPathConfig = pathConfig;
}

void ExportImportUtil::addLogCreateResource(const QString &str)
{
    mLogCreateResource->appendText(str);
}

QString ExportImportUtil::loggingFilePath() const
{
    return mLogCreateResource->logPath();
}

void ExportImportUtil::setExtractPath(const QString &extractPath)
{
    mExtractPath = extractPath;
}

QString ExportImportUtil::extractPath() const
{
    return mExtractPath;
}
