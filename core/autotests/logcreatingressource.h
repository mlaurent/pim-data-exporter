/*
   Copyright (C) 2020 Laurent Montel <montel@kde.org>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; see the file COPYING.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef LOGCREATINGRESOURCE_H
#define LOGCREATINGRESOURCE_H

#include <QObject>
#include <Akonadi/KMime/SpecialMailCollections>
class QTemporaryFile;
class LogCreatingResource : public QObject
{
    Q_OBJECT
public:
    explicit LogCreatingResource(QObject *parent = nullptr);
    ~LogCreatingResource();
    void appendText(const QString &str);
    Q_REQUIRED_RESULT QString logPath() const;
    Q_REQUIRED_RESULT QString logCreateResource(const QString &resources, const QString &name, const QMap<QString, QVariant> &settings, bool synchronizeTree);
    void logSynchronizeResource(const QStringList &lst);
    void logRegisterSpecialCollection(Akonadi::SpecialMailCollections::Type type, qint64 colId);
private:
    QTemporaryFile *mTmpLogFile = nullptr;
    int mResourceIndex = 0;
};

#endif // LOGCREATINGRESOURCE_H
