/*
   Copyright (C) 2013-2020 Laurent Montel <montel@kde.org>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; see the file COPYING.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef SYNCHRONIZERESOURCEJOB_H
#define SYNCHRONIZERESOURCEJOB_H

#include <QObject>
#include <QStringList>
#include "pimdataexporter_export.h"
class KJob;
class PIMDATAEXPORTER_EXPORT SynchronizeResourceJob : public QObject
{
    Q_OBJECT
public:
    explicit SynchronizeResourceJob(QObject *parent = nullptr);
    ~SynchronizeResourceJob();

    void start();
    void setListResources(const QStringList &resources);
    void setSynchronizeOnlyCollection(bool onlyCollection);

Q_SIGNALS:
    void synchronizationFinished();
    void synchronizationInstanceDone(const QString &name, const QString &identifier);
    void synchronizationInstanceFailed(const QString &);

private:
    void slotSynchronizationFinished(KJob *);
    void slotNextSync();

    QStringList mListResources;
    int mIndex = 0;
    bool mOnlyCollection = true;
};

#endif // SYNCHRONIZERESOURCEJOB_H
